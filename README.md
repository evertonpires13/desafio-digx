# desafio-digx



## Baixar o projeto
Para baixar o projeto proceda da sesguinte maneira:
```
git clone https://gitlab.com/evertonpires13/desafio-digx.git
```

## Executar Sonar
Sonar valida e testa o código fonte do projeto. Mostrando clean Code e testes unitários
```
mvn clean install sonar:sonar -Dsonar.host.url=http://localhost:9000  -Dsonar.login=admin -Dsonar.password=admin123
```


## Verificar Swagger (URL)
O swagger mostra as url's válidas para testar o projeto
```
http://localhost:8080/swagger-ui/#/
```
 
## Executar o projeto
Para executar o projeto WEB, execute o comando abaixo

```
mvn clean spring-boot:run
```
 

## Observações
<ul>
    <li>O Projeto está rodando normalmente</li>
    <li>
        Criei uma interface com o nome ValidadorGenerico para que assim seja mais simples criar novas regras ou excluir regras. Para isto deveremos proceder da seguinte maneira.
        <ul>
            <li>Crie uma classe extendendo ValidadorGenerico e sobreponha o método calcular passando a familia como parâmetro.</li>
            <li>Abra a classe ValidadorBusinnes e dentro do método pontuador, adicione uma linha de código logo abaixo da linha 38 List<ValidadorGenerico> pontuador = new ArrayList<>();</li>
            <li>A linha pode seguir o seguinte modelo :  pontuador.add(new ClasseNova());</li>
        </ul>
    </li>
    <li>Uma vez as instruções acima sejam seguidas, uma nova regra será implementada de maneira simples e direta</li>
    <li>Eu poderia melhorar a lógica das regras existentes, mas as alterações em si não surtiriam tanto efeito.</li>
    <li>Como não está claro se o projeto seria web, desktop, mobile, etc, criei um pacote businnes para que todas as lógicas de negócio fossem direcionadas para lá, para seguir padrões de projetos conhecidos.</li>
    <li>Criei também um arquivo CasasPopulares com um metodo main. Esta classe também pode ser executada e testado o sistema.</li>
</ul>
 


Sem mais...

Obrigado

Everton Pires
