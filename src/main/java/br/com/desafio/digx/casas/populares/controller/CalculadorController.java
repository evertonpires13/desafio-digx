package br.com.desafio.digx.casas.populares.controller;

import br.com.desafio.digx.casas.populares.businnes.ValidadorBusinnes;
import br.com.desafio.digx.casas.populares.dto.Familia;
import br.com.desafio.digx.casas.populares.dto.Mensagem;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Log4j2
@RestController
@RequestMapping("/calcular")
@Api(value = "Calculador")
public class CalculadorController {

    @Autowired
    private ValidadorBusinnes businnes;

    @PostMapping("/ordenar")
    @ApiOperation(value = "Ordenar Lista de familias")
    public ResponseEntity<Mensagem> ordenar(@PathVariable List<Familia> familias) {
        Mensagem retorno = null;
        String mensagem = null;

        try {
            if (familias != null) {
                familias = businnes.ordenar(familias);
            }
        } catch (Exception e) {
            mensagem = "Erro inesperado : " + e;
        }

        retorno = Mensagem.builder().familiaOrdenada(familias).msg(mensagem).build();
        return new ResponseEntity<>(retorno, getHeaders(), HttpStatus.OK);

    }

    private MultiValueMap<String, String> getHeaders() {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("teste-api-version", "1.0");
        headers.add("X-api-key", "1.0");
        return headers;
    }

}
