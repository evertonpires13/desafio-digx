package br.com.desafio.digx.casas.populares.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class Pessoa {

    private String nome;
    private int idade;
    private float salario;

}
