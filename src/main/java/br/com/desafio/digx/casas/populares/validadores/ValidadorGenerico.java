package br.com.desafio.digx.casas.populares.validadores;

import br.com.desafio.digx.casas.populares.dto.Familia;

public interface ValidadorGenerico {

    int calcular(Familia familia);
}
