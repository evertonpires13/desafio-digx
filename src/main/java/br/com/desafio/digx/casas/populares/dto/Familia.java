package br.com.desafio.digx.casas.populares.dto;

import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
 @Builder
public class Familia implements Comparable<Familia> {

    private Pessoa pretendente;
    private Pessoa conjuge;
    private List<Pessoa> dependentes;
    private Integer pontos;

    @Override
    public int compareTo(Familia compareFamilia) {
        if (this.pontos > compareFamilia.getPontos()) {
            return -1;
        } else {
            if (this.pontos < compareFamilia.getPontos()) {
                return 1;
            } else {
                return 0;
            }
        }

    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return "Pretendente: " + getPretendente().getNome() + " Pontos: " + getPontos() + " / "; 
    }
    
    
    

}
