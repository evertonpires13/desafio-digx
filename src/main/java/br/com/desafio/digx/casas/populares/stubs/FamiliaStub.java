package br.com.desafio.digx.casas.populares.stubs;

import br.com.desafio.digx.casas.populares.dto.Familia;
import br.com.desafio.digx.casas.populares.dto.Pessoa;

import java.util.ArrayList;
import java.util.List;

public class FamiliaStub {
private FamiliaStub(){}
    public static Familia familiaRendaAcima1500SemFilhos() {

        Pessoa adulto1 = PessoaStub.adultoComSalario1000();
        adulto1.setNome(adulto1.getNome() + " - familiaRendaAcima1500SemFilhos");
        Pessoa adulto2 = PessoaStub.adultoComSalario1000();

        return Familia.builder().conjuge(adulto2).pretendente(adulto1).build();

        // zero ponto
    }

    public static Familia familiaRendaAcima1500Com2FilhosValidos() {

        Pessoa adulto1 = PessoaStub.adultoComSalario1000();
        adulto1.setNome(adulto1.getNome() + " - familiaRendaAcima1500Com2FilhosValidos");
        Pessoa adulto2 = PessoaStub.adultoComSalario1000();

        Pessoa filho2 = PessoaStub.filhoMenorSemSalario();
        Pessoa filho3 = PessoaStub.filhoMenorSemSalario();

        List<Pessoa> dependentes = new ArrayList<>();

        dependentes.add(filho2);
        dependentes.add(filho3);

        return Familia.builder().conjuge(adulto2).pretendente(adulto1).dependentes(dependentes).build();

    }

    public static Familia familiaRendaAcima1500Com4Filhos() {
        Pessoa adulto1 = PessoaStub.adultoComSalario1000();
        adulto1.setNome(adulto1.getNome() + " - familiaRendaAcima1500Com4Filhos");
        Pessoa adulto2 = PessoaStub.adultoComSalario1000();

        Pessoa filho1 = PessoaStub.filhoMaiorComSalario1000();
        Pessoa filho2 = PessoaStub.filhoMenorSemSalario();
        Pessoa filho3 = PessoaStub.filhoMenorSemSalario();
        Pessoa filho4 = PessoaStub.filhoMenorSemSalario();

        List<Pessoa> dependentes = new ArrayList<>();
        dependentes.add(filho1);
        dependentes.add(filho2);
        dependentes.add(filho3);
        dependentes.add(filho4);

        return Familia.builder().conjuge(adulto2).pretendente(adulto1).dependentes(dependentes).build();

    }

    public static Familia familiaRenda1300SemFilhos() {
        Pessoa adulto1 = PessoaStub.adultoComSalario1000();
        adulto1.setNome(adulto1.getNome() + " - familiaRenda1300SemFilhos");
        Pessoa adulto2 = PessoaStub.adultoSemSalario();

        return Familia.builder().conjuge(adulto2).pretendente(adulto1).build();

    }

    public static Familia familiaRenda1300Com02Filhos() {
        Pessoa adulto1 = PessoaStub.adultoComSalario1000();
         adulto1.setNome(adulto1.getNome() + " - familiaRenda1300Com02Filhos");
        Pessoa adulto2 = PessoaStub.adultoSemSalario();

        Pessoa filho1 = PessoaStub.filhoMaiorComSalario200();
        Pessoa filho2 = PessoaStub.filhoMenorSemSalario();
        Pessoa filho3 = PessoaStub.filhoMenorSemSalario();

        List<Pessoa> dependentes = new ArrayList<>();
        dependentes.add(filho1);
        dependentes.add(filho2);
        dependentes.add(filho3);

        return Familia.builder().conjuge(adulto2).pretendente(adulto1).dependentes(dependentes).build();

    }

    public static Familia familiaRenda1300Com04Filhos() {
        Pessoa adulto1 = PessoaStub.adultoComSalario1000();
        adulto1.setNome(adulto1.getNome() + " - familiaRenda1300Com04Filhos");
        Pessoa adulto2 = PessoaStub.adultoSemSalario();

        Pessoa filho1 = PessoaStub.filhoMaiorComSalario200();
        Pessoa filho2 = PessoaStub.filhoMenorSemSalario();
        Pessoa filho3 = PessoaStub.filhoMenorSemSalario();
        Pessoa filho4 = PessoaStub.filhoMenorSemSalario();

        List<Pessoa> dependentes = new ArrayList<>();
        dependentes.add(filho1);
        dependentes.add(filho2);
        dependentes.add(filho3);
        dependentes.add(filho4);

        return Familia.builder().conjuge(adulto2).pretendente(adulto1).dependentes(dependentes).build();

    }

    public static Familia familiaRenda700Com04Filhos() {
        Pessoa adulto1 = PessoaStub.adultoComSalario400();
        adulto1.setNome(adulto1.getNome() + " - familiaRenda700Com04Filhos");
        Pessoa adulto2 = PessoaStub.adultoComSalario400();

        Pessoa filho1 = PessoaStub.filhoMaiorSemSalario();
        Pessoa filho2 = PessoaStub.filhoMenorSemSalario();
        Pessoa filho3 = PessoaStub.filhoMenorSemSalario();
        Pessoa filho4 = PessoaStub.filhoMenorSemSalario();

        List<Pessoa> dependentes = new ArrayList<>();
        dependentes.add(filho1);
        dependentes.add(filho2);
        dependentes.add(filho3);
        dependentes.add(filho4);

        return Familia.builder().conjuge(adulto2).pretendente(adulto1).dependentes(dependentes).build();

    }

    public static Familia familiaRenda700Com02Filhos() {
        Pessoa adulto1 = PessoaStub.adultoComSalario400();
        adulto1.setNome(adulto1.getNome() + " - familiaRenda700Com02Filhos");
        Pessoa adulto2 = PessoaStub.adultoComSalario400();

        Pessoa filho1 = PessoaStub.filhoMaiorSemSalario();
        Pessoa filho2 = PessoaStub.filhoMenorSemSalario();

        List<Pessoa> dependentes = new ArrayList<>();
        dependentes.add(filho1);
        dependentes.add(filho2);

        return Familia.builder().conjuge(adulto2).pretendente(adulto1).dependentes(dependentes).build();

    }

    public static Familia familiaRenda700SemFilhos() {
        Pessoa adulto1 = PessoaStub.adultoComSalario400();
        adulto1.setNome(adulto1.getNome() + " - familiaRenda700SemFilhos");
        Pessoa adulto2 = PessoaStub.adultoComSalario400();

        return Familia.builder().conjuge(adulto2).pretendente(adulto1).build();

    }

}
