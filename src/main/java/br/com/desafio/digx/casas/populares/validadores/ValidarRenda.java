package br.com.desafio.digx.casas.populares.validadores;

import br.com.desafio.digx.casas.populares.dto.Familia;
import br.com.desafio.digx.casas.populares.dto.Pessoa;

import java.util.List;

public class ValidarRenda implements ValidadorGenerico {
    @Override
    public int calcular(Familia familia) {

        float rendaPretendente = familia.getConjuge().getSalario();
        float rendaConjuge = familia.getPretendente().getSalario();
        float rendaDependentes = salarioDependentes(familia.getDependentes());
        float salarios = rendaPretendente + rendaConjuge + rendaDependentes;

        if (salarios < 900) {
            return 5;
        } else {
            if (salarios < 1500) {
                return 3;
            } else {
                return 0;
            }
        }
    }

    private float salarioDependentes(List<Pessoa> dependentes) {
        float salarios = 0;
        if (dependentes != null) {
            for (Pessoa p : dependentes) {
                salarios += p.getSalario();
            }
        }
        return salarios;
    }


}
