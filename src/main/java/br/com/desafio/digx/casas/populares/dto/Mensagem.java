package br.com.desafio.digx.casas.populares.dto;

import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Mensagem {

    private String msg;
    private List<Familia> familiaOrdenada;

}
