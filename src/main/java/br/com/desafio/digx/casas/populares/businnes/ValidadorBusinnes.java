package br.com.desafio.digx.casas.populares.businnes;

import br.com.desafio.digx.casas.populares.dto.Familia;
import br.com.desafio.digx.casas.populares.validadores.ValidadorGenerico;
import br.com.desafio.digx.casas.populares.validadores.ValidarDependentes;
import br.com.desafio.digx.casas.populares.validadores.ValidarRenda;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class ValidadorBusinnes {

    public List<Familia> ordenar(List<Familia> familias) {
        if (familias != null) {
            List<Familia> retorno = new ArrayList<>();
            for (Familia f : familias) {
                retorno.add(calculePontos(f));
            }
            Collections.sort(retorno);
            familias = retorno;
        }
        return familias;
    }

    public int recursivo(List<ValidadorGenerico> t, int posicao, Familia familia){
        if ( t.size()   == posicao){
            return 0;
        }else{
             return  t.get(posicao).calcular(familia)+ recursivo(t, ++posicao, familia ) ;
        }
    }

    private Familia calculePontos(Familia familia) {

        List<ValidadorGenerico> pontuador = new ArrayList<>();
        pontuador.add(new ValidarDependentes());
        pontuador.add(new ValidarRenda());

        int pontos = recursivo(pontuador, 0, familia);
        familia.setPontos(pontos);
        return familia;
    }
}
