package br.com.desafio.digx.casas.populares.validadores;

import br.com.desafio.digx.casas.populares.dto.Familia;
import br.com.desafio.digx.casas.populares.dto.Pessoa;

import java.util.List;

public class ValidarDependentes implements ValidadorGenerico {
    @Override
    public int calcular(Familia familia) {

        if (familia.getDependentes() != null) {
            List<Pessoa> dependentes = familia.getDependentes();
            int filhos = 0;

                for (Pessoa p : dependentes) {
                    if (p.getIdade() <= 18) {
                        filhos++;
                    }
                }

            if (filhos > 0 && filhos < 3) {
                return 2;
            } else {
                if (filhos > 2) {
                    return 3;
                } else {
                    return 0;
                }
            }
        } else {
            return 0;
        }
    }
}
