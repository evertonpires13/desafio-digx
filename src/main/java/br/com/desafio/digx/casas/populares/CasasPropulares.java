package br.com.desafio.digx.casas.populares;

import br.com.desafio.digx.casas.populares.businnes.ValidadorBusinnes;
import br.com.desafio.digx.casas.populares.dto.Familia;
import br.com.desafio.digx.casas.populares.stubs.FamiliaStub;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.List;

@Log4j2
public class CasasPropulares {

    public static void main(String[] args){

        Familia familia1 = FamiliaStub.familiaRenda1300Com02Filhos();
        Familia familia2 = FamiliaStub.familiaRenda1300Com04Filhos();
        Familia familia3 = FamiliaStub.familiaRenda1300SemFilhos();
        Familia familia4 = FamiliaStub.familiaRenda700Com02Filhos();
        Familia familia5 = FamiliaStub.familiaRenda700Com04Filhos();
        Familia familia6 = FamiliaStub.familiaRenda700SemFilhos();
        Familia familia7 = FamiliaStub.familiaRendaAcima1500Com2FilhosValidos();
        Familia familia8 = FamiliaStub.familiaRendaAcima1500Com4Filhos();
        Familia familia9 = FamiliaStub.familiaRendaAcima1500SemFilhos();

        List<Familia> familias = new ArrayList<>();
        familias.add(familia1);
        familias.add(familia2);
        familias.add(familia3);
        familias.add(familia4);
        familias.add(familia5);
        familias.add(familia6);
        familias.add(familia7);
        familias.add(familia8);
        familias.add(familia9);

        ValidadorBusinnes validadorBusinnes = new ValidadorBusinnes();

        log.info("Antes : " + familias);
        familias = validadorBusinnes.ordenar(familias);
        log.info("Depois : " + familias);

    }

}
