package br.com.desafio.digx.casas.populares.stubs;

import br.com.desafio.digx.casas.populares.dto.Pessoa;

public class PessoaStub {
    private PessoaStub(){}

    public static Pessoa adultoComSalario1000() {
        return Pessoa.builder().idade(45).nome("adultoComSalario1000 1").salario(1000F).build();
    }
    
     public static Pessoa adultoComSalario400() {
        return Pessoa.builder().idade(45).nome("adultoComSalario400 2").salario(400F).build();
    }
    
     public static Pessoa adultoSemSalario() {
        return Pessoa.builder().idade(45).nome("adultoSemSalario 3").salario(0F).build();
    }

    public static Pessoa filhoMenorSemSalario() {
        return Pessoa.builder().idade(15).nome("filhoMenorSemSalario 11").salario(0F).build();
    }

    public static Pessoa filhoMaiorComSalario1000() {
        return Pessoa.builder().idade(19).nome("filhoMaiorComSalario1000 11").salario(1000F).build();
    }

    public static Pessoa filhoMaiorComSalario200() {
        return Pessoa.builder().idade(19).nome("filhoMaiorComSalario200 11").salario(200F).build();
    }

    public static Pessoa filhoMaiorSemSalario() {
        return Pessoa.builder().idade(19).nome("filhoMaiorSemSalario 11").salario(0F).build();
    }

}
