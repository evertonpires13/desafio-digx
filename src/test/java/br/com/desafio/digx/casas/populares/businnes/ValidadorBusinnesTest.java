package br.com.desafio.digx.casas.populares.businnes;

import static org.junit.jupiter.api.Assertions.assertEquals;
import br.com.desafio.digx.casas.populares.businnes.stubs.FamiliaStub;
import br.com.desafio.digx.casas.populares.dto.Familia;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ValidadorBusinnesTest {

    @Autowired
    ValidadorBusinnes validadorBusinnes;
    @Test
    void ordenar() {

        Familia familia1 = FamiliaStub.familiaRenda1300Com02Filhos();
        Familia familia2 = FamiliaStub.familiaRenda1300Com04Filhos();
        Familia familia3 = FamiliaStub.familiaRenda1300SemFilhos();
        Familia familia4 = FamiliaStub.familiaRenda700Com02Filhos();
        Familia familia5 = FamiliaStub.familiaRenda700Com04Filhos();
        Familia familia6 = FamiliaStub.familiaRenda700SemFilhos();
        Familia familia7 = FamiliaStub.familiaRendaAcima1500Com2FilhosValidos();
        Familia familia8 = FamiliaStub.familiaRendaAcima1500Com4Filhos();
        Familia familia9 = FamiliaStub.familiaRendaAcima1500SemFilhos();

        List<Familia> familias = new ArrayList<>();
        familias.add(familia1);
        familias.add(familia2);
        familias.add(familia3);
        familias.add(familia4);
        familias.add(familia5);
        familias.add(familia6);
        familias.add(familia7);
        familias.add(familia8);
        familias.add(familia9);


        familias = validadorBusinnes.ordenar(familias);

        assertEquals(8, familias.get(0).getPontos() );
        assertEquals(0, familias.get(familias.size()-1).getPontos()  );

    }


}
